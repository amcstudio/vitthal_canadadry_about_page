;(function () {
	
	'use strict';

	$(document).ready(function(){
	    $(window).scroll(function(){
	        $(".scrollFadeOut").css("opacity", 1 - $(window).scrollTop() / $('.scrollFadeOut').height());
	    });

	    $('.icon-ham').click(function(e){
	    	e.preventDefault();
			$(".menu").slideToggle(500)
		})


	});


}());